package com.cipher;

import java.util.Random;
import java.util.regex.Pattern;

import com.cipher.gui.Frame;

public class Cipher {
    public static Converter converter;
    public static int variance = 32;
    public static boolean oldVersion = false;
    
    public static void main(String args[]){
        converter = new Converter("tZ|[Vi*;I]EsX6M>vnCu _mqOB49Jf5-kPW7),1rQRHh}($gFzdTD!<Awo=:@2?+0c%a3GKUx^.p/ylY8bj{NS&Le#", "KZP-H,^SDUNXV#CJ%TYGBOA!LR=F/W.&Q$M@I*E");
        
        if(args.length > 0){
            String msg = "", result = "";
            int count = 0;
            boolean encipher = args[0].equalsIgnoreCase("ENCIPHER") ? true : false;
            if(isTestEnv()){
                if(args.length <= 1) System.exit(3);
                for(String arg : args){
                    if(arg.equals(args[0])) continue;
                    if(count > 0 && count != 0)
                        msg += " " + arg;
                    else
                        msg += arg;
                    count++;
                }
            } else {
                String type = encipher ? "encipher" : "decipher";
                System.out.println("Enter string to " + type);
                msg = System.console().readLine();
            }
            
            try{
                if(encipher){
                    System.out.println("Enciphered message: ");
                    if(!oldVersion)
                        variance = msg.length();
                    result = cipher(converter, msg, 0);
                } else{
                    System.out.println("Deciphered message: ");
                    if(!oldVersion)
                        variance = msg.length() / 2;
                    result = cipher(converter, msg, 1);
                }
            } catch (Exception e){
                e.printStackTrace();
            }
            
            System.out.println(result);
            try {
                System.out.println(cipher(converter, result, 1));
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.exit(3);
        } else 
            new Frame();
    }
    
    public static boolean isTestEnv() {
        return System.getenv("eclipse44") == null ? false : true;
    }
    
    public static String cipher(Converter converter, String msg, int type) throws Exception{
        Random r = new Random();
        String message = "", encryptionKey = "";
        int switchType = 0, methodType = 0;
        int[] asa = new int[variance];
        
        switch(type){
            case 0:
                for(int i = 0; i < asa.length; i++)
                    asa[i] = r.nextInt(converter.keyAlphabet.size());
                break;
            case 1:
                String asaNum = msg.substring(0, variance);
                msg = msg.substring(variance);
                for(int i = 0; i < asaNum.length(); i++){
                    char ch = asaNum.charAt(i);
                    if(!Pattern.matches("[A-Za-z0-9!@#$%^&*,./=-]*", String.valueOf(ch))) continue;
                    asa[i] = converter.decryptKey(String.valueOf(ch));
                }
                break;
        }
        
        for(int i = 0; i < msg.length(); i++){
            char ch = msg.charAt(i);
            int chNum = converter.convertCharToNum(ch);
            
            //TODO: Implement method that supports backslashes, single quotes and other ASCII characters that currently do not work.
            
            if(type == 1)
                if(methodType >= 1)
                    methodType = 0;
                else
                    methodType++;
            chNum = converter.encryptCharByInt(chNum, asa[switchType], methodType);
            if(type == 0)
                if(methodType >= 1)
                    methodType = 0;
                else
                    methodType++;
            
            if(switchType >= (variance - 1))
                switchType = 0;
            else
                switchType++;
            message += converter.convertNumToChar(chNum);
        }
        if(type == 0){
            for(int k = 0; k < asa.length; k++)
                encryptionKey += converter.encryptKey(asa[k]);
            return encryptionKey + message;
        } else
            return message;
        
    }
}
