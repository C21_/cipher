package com.cipher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.regex.Pattern;

public class Converter {
    public HashMap<Integer, String> alphabet;
    public int symbolsInAlphabet = 0;
    public HashMap<Integer, ArrayList<String>> keyAlphabet;
    
    public Converter(String alphabet, String keyAlphabet){
        this.keyAlphabet = new HashMap<Integer, ArrayList<String>>();
        this.alphabet = new HashMap<Integer, String>();
        
        symbolsInAlphabet = alphabet.length();
        initAlphabet(alphabet);
        initKeyAlphabet(keyAlphabet);
    }
    
    public void initKeyAlphabet(String symbols){
        int count = 0, pos = 0;
        ArrayList<String> charlist = new ArrayList<String>();
        
        for(int i = 0; i < symbols.length(); i++){
            charlist.add(String.valueOf(symbols.charAt(i)));
            if(count >= 2){
                keyAlphabet.put(pos, charlist);
                
                charlist = new ArrayList<String>();
                count = 0;
                pos++;
            } else
                count++;
        }
    }
    
    public void initAlphabet(String symbols){
        alphabet.put(0, "");
        for(int i = 1; i < symbols.length(); i++)
            alphabet.put(i, String.valueOf(symbols.charAt(i)));
    }
    
    public String encryptKey(int i){
        Random r = new Random();
        int type = r.nextInt(100) + 1;
        int lower = r.nextInt(100) + 1;
        if(type <= 20 && i > -1 && i < 10)
            return String.valueOf(i);
        else if(lower < 50)
            return keyAlphabet.get(i).get(r.nextInt(3)).toLowerCase();
        else
            return keyAlphabet.get(i).get(r.nextInt(3));
        
    }
    
    public int decryptKey(String i){
        if(!(i.length() == 1)) return -1;
        if(Pattern.matches("[0-9]*", i))
            return Integer.valueOf(i);
        else
            for(int e : keyAlphabet.keySet())
                if(keyAlphabet.get(e).contains(i.toUpperCase()))
                    return e;
        return -1;
    }
    
    public String convertNumToChar(int i){
        return alphabet.get(i);
    }
    
    public int convertCharToNum(char ch){
        String c = String.valueOf(ch);
        for(int i : alphabet.keySet())
            if(alphabet.get(i).equals(c)) return i;
        return -1;
    }
    
    public int encryptCharByInt(int charNumber, int adjustment, int type){
        int adjustedNum = 0, symbols = symbolsInAlphabet - 1;
        if(type == 1)
            adjustedNum = charNumber - adjustment;
        else 
            adjustedNum = charNumber + adjustment;
        if(adjustedNum > symbols && adjustedNum > 0)
            adjustedNum = adjustedNum - symbols;
        else if(adjustedNum < 1)
            adjustedNum = adjustedNum + symbols;
        
        return adjustedNum;
    }
}
