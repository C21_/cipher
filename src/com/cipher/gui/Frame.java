package com.cipher.gui;

import java.awt.Container;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import com.cipher.Cipher;
import com.cipher.Converter;

public class Frame extends JFrame implements ActionListener{
    private static final long serialVersionUID = 481437945235739329L;
    static JTextArea text;
    static JButton encipher, decipher;
    static JLabel status;
    static JCheckBox copy;
    static JPanel panel;
    
    static Converter converter;
    
    public Frame(){
        converter = new Converter("tZ|[Vi*;I]EsX6M>vnCu _mqOB49Jf5-kPW7),1rQRHh}($gFzdTD!<Awo=:@2?+0c%a3GKUx^.p/ylY8bj{NS&Le#", "KZP-H,^SDUNXV#CJ%TYGBOA!LR=F/W.&Q$M@I*E");
        
        setTitle("cipher d31b8.1508150944353");
        setSize(450, 450);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        panel = new JPanel();
        text = new JTextArea();
        decipher = new JButton("Decipher");
        encipher = new JButton("Encipher");
        status = new JLabel();
        copy = new JCheckBox("Copy to Clipboard");
        
        decipher.addActionListener(this);
        encipher.addActionListener(this);
        
        text.setLineWrap(true);
        text.setWrapStyleWord(true);
        
        Container pane = getContentPane();
        GroupLayout layout = new GroupLayout(pane);
        
        pane.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(text)
                .addGroup(layout.createSequentialGroup()
                        .addComponent(encipher)
                        .addComponent(decipher)
                        .addComponent(copy)
                        )
                .addComponent(status)
                );
        
        layout.setVerticalGroup(
                layout.createSequentialGroup()
                .addComponent(text)
                .addGroup(layout.createParallelGroup()
                        .addComponent(encipher)
                        .addComponent(decipher)
                        .addComponent(copy)
                        )
                .addComponent(status)
                );
        
        setVisible(true);
    }
    
    @Override
    public void actionPerformed(ActionEvent e){
        if(e.getSource() instanceof JButton){
            JButton clickedButton = (JButton) e.getSource();
            try{
                switch(clickedButton.getText()){
                    case "Encipher":
                        if(!Cipher.oldVersion)
                            Cipher.variance = Frame.text.getText().length();
                        Frame.text.setText(Cipher.cipher(converter, Frame.text.getText(), 0));
                        Frame.status.setText(!copy.isSelected() ? "Text has been enciphered." : "Enciphered text copied to clipboard");
                        break;
                    case "Decipher":
                        if(!Cipher.oldVersion)
                            Cipher.variance = Frame.text.getText().length() / 2;
                        Frame.text.setText(Cipher.cipher(converter, Frame.text.getText(), 1));
                        Frame.status.setText(!copy.isSelected() ? "Text has been deciphered." : "Deciphered text copied to clipboard");
                        break;
                }
            } catch(Exception ex){
                Frame.status.setText("An error has occured.");
                ex.printStackTrace();
            }
            if(copy.isSelected()){
                StringSelection selection = new StringSelection(Frame.text.getText());
                Clipboard board = Toolkit.getDefaultToolkit().getSystemClipboard();
                board.setContents(selection, null);
            }
        }
    }
    
}